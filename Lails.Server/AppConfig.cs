﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lails.Server
{
    public class AppConfig
    {
        public static AppConfig Default { get; private set; }

        static AppConfig()
        {
            Default = Load<AppConfig>();
            //增加前缀
            AppLogger.Info($"服务前缀为：{Default.ServicePrefix}");
            if (!string.IsNullOrEmpty(Default.ServicePrefix))
            {
                Default.ServiceName = $"{Default.ServicePrefix}_{Default.ServiceName}";
                Default.DisplayName = $"{Default.ServicePrefix}_{Default.DisplayName}";
            }
        }

        public AppConfig()
        {
            var name = Process.GetCurrentProcess().ProcessName;
            this.ServiceName = this.ServiceName ?? name;
            this.Description = this.Description ?? name;
            this.DisplayName = this.DisplayName ?? name;
        }

        /// <summary>
        /// 服务名称前缀，用于标识一类服务，显示名称为:{ServicePrefix}_{ServiceName}
        /// </summary>
        public string ServicePrefix { get; set; }
        /// <summary>
        /// 服务的描述信息
        /// </summary>
        public string ServiceName { get; set; }
        /// <summary>
        /// 服务的显示名称
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// 服务的名称
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 设置该值时，表示开启WebAPI服务
        /// </summary>
        public int HttpPort { get; set; }

        public static T Load<T>() where T : AppConfig
        {
            var config = Lails.Config.ConfigLoader.Load<T>("Config/config.json");
            return config;
        }
    }
}
